package com.flashlight;

import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.hardware.Camera;
import android.widget.Button;


public class MainActivity extends Activity {
    public static Camera cam = null;
    Boolean on_off = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_exit:
                android.os.Process.killProcess(android.os.Process.myPid());
                //System.exit(0);
                break;
            default:
                return false;
        }
        return true;
    }

    public void onDestroy() {
        try {
            moveTaskToBack(true);
            super.onDestroy();
            android.os.Process.killProcess(android.os.Process.myPid());
            //System.exit(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void flash_on(View v){
        Button btn = (Button)findViewById(R.id.button);

        if(!on_off){
            on_off=true;
            btn.setBackgroundResource(R.drawable.lighter);

            flash_on();

        }else{
            on_off=false;
            btn.setBackgroundResource(R.drawable.darker);
            flash_off();
        }
    }

    public void flash_on(){

        try {
                cam = Camera.open();
                Camera.Parameters p = cam.getParameters();
                p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                cam.setParameters(p);
                cam.startPreview();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void flash_off() {
        try {
            Camera.Parameters p = cam.getParameters();
            p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            if (cam != null) {
                cam.stopPreview();

                cam.release();

                cam = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
